const express = require("express");
const http = require("http");
const cors = require('cors');
const socketIo = require("socket.io");

const port = process.env.PORT || 4000;
const index = require("./routes/index");

const app = express();
app.use(index);
app.use(cors());

const server = http.createServer(app);
let interval;

const io = require("socket.io")(server, {
    cors: {
      origin: "*",    
  handlePreflightRequest: (req, res) => {
    res.writeHead(200, {
      "Access-Control-Allow-Origin": '*',
      "Access-Control-Allow-Methods": 'GET,POST',
      "Access-Control-Allow-Headers": 'my-custom-header',
      "Access-Control-Allow-Credentials": false
    });
  res.end();
  }}
});

const SerialPort = require('serialport');
var COM = 'COM3';
var serialPort = new SerialPort(COM, {
  baudRate: 9600,
  parser: new SerialPort.parsers.Readline("\n")
});

var dataString;
serialPort.on('open', function() {
  console.log('open');

  serialPort.on('data', function(data) {
    dataString = Number(data.toString());
  });
});

io.on("connection", (socket) => {
  console.log("New client connected");
  if (interval) {
    clearInterval(interval);
  }
  interval = setInterval(() => getApiAndEmit(socket), 1000);
  socket.on("disconnect", () => {
    console.log("Client disconnected");
    clearInterval(interval);
  });
});
var tick = 0;
const getApiAndEmit = socket => {
  const response = dataString;
  // Emitting a new message. Will be consumed by the client
  console.log(response);
  socket.emit('FromAPI', {
    name: tick++,
    value: response
  });

  socket.emit("FromAPI", response);
};

server.listen(port, () => console.log(`Listening on port ${port}`));