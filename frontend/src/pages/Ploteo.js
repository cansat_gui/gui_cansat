import React, {useEffect, useState} from 'react';
import socketIOClient from "socket.io-client"
import {
  Line,
  LineChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from 'recharts';

const ENDPOINT = "http://localhost:4000";

const socket = socketIOClient(ENDPOINT);

const Ploteo = () => {

  const [response, setResponse] = useState([]);

  useEffect(() => {
    socket.on('FromAPI', data => {
      setResponse(currentData => [...currentData, data]);
    });
  }, []);
 
  return (
    <div>
      <LineChart
        width={500}
        height={300}
        data={response}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
        >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis 
        type='number' 
        dataKey="name" 
        allowDataOverflow = 'false'
        domain={['dataMax-10', 'dataMax + 10']}
        
        />
        <YAxis type='number' domain={[0, 'dataMax+5']}/>
        <Tooltip />
        <Legend />
        <Line 
        type="monotone" 
        dataKey="value" 
        stroke="#8884d8" 
        activeDot={{ r: 8 }} 
        connectNulls='true' 
        isAnimationActive='true' 
        />
      </LineChart>
    </div> 
  );
}

export default Ploteo;